/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeframe;

/**
 *
 * @author MSI
 */
public class Rectangle extends Shape {

    private double Width;
    private double Length;

    public Rectangle(double Width, double Length) {
        super("Circle");
        this.Width = Width;
        this.Length = Length;
    }

    public double getWidth() {
        return Width;
    }

    public void setWidth(double Width) {
        this.Width = Width;
    }

    public double getLength() {
        return Length;
    }

    public void setLength(double Length) {
        this.Length = Length;
    }

    @Override
    public double calArea() {
        return Width * Length;
    }

    @Override
    public double calPerimeter() {
        return (Width + Length) * 2;
    }

}
