/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author MSI
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWidth = new JLabel("Width :", JLabel.CENTER);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);

        JLabel lblLength = new JLabel("Length :", JLabel.CENTER);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5, 20);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        frame.add(lblLength);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        frame.add(txtWidth);

        final JTextField txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 20);
        frame.add(txtLength);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Width = ??? Length = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String Width = txtWidth.getText();
                    String Length = txtLength.getText();

                    double w = Double.parseDouble(Width);
                    double l = Double.parseDouble(Length);

                    Rectangle rectangle = new Rectangle(w, l);
                    lblResult.setText("Width : " + String.format("%.2f", rectangle.getWidth()) + "Length : " + String.format("%.2f", rectangle.getLength()) + "  Area : " + String.format("%.2f", rectangle.calArea()) + "  Perimeter : " + String.format("%.2f", rectangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtLength.setText("");
                    txtWidth.requestFocus();
                    txtLength.requestFocus();

                }
            }

        });

        frame.setVisible(true);
    }

}
