/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author MSI
 */
public class TriangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("Base :", JLabel.CENTER);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        JLabel lblHeight = new JLabel("Height :", JLabel.CENTER);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 20);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 20);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Base = ??? Height = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String Base = txtBase.getText();
                    String Height = txtHeight.getText();

                    double b = Double.parseDouble(Base);
                    double h = Double.parseDouble(Height);

                    Triangle triangle = new Triangle(b, h);
                    lblResult.setText("Base : " + String.format("%.2f", triangle.getBase()) + "Height : " + String.format("%.2f", triangle.getHeight()) + "  Area : " + String.format("%.2f", triangle.calArea()) + "  Perimeter : " + String.format("%.2f", triangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                    txtHeight.requestFocus();

                }
            }

        });

        frame.setVisible(true);
    }
}
